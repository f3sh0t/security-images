from datetime import datetime
import requests
import os

defectdojo_url = os.environ.get('SEC_DD_URL', '')
defectdojo_key = os.environ.get('SEC_DD_KEY', '')

attach_file_flag = os.environ.get('ATTACH_FILE', True)

scanner_names = [
    'Trivy Scan',
    'Anchore Grype',
    'Dependency Check Scan'
]

package_filename = os.environ.get('DD_ATTACHMENT_FILE_NAME', '')
print(f'Got package_filename: {package_filename}')
package_filepath = os.environ.get('DD_ATTACHMENT_FILE_PATH', '')
print(f'Got package_filepath: {package_filepath}')
engagement_id = os.environ.get('DD_ENGAGEMENT_ID', '')
print(f'Got engagement_id: {engagement_id}')


def get_scanners_ids():
    result = list()
    for scanner_name in scanner_names:
        try:
            result.append(
                requests.get(
                    url=f'{defectdojo_url}/api/v2/test_types/?name={scanner_name.replace(" ", "%20")}',
                    headers={
                        'Authorization': f'Token {defectdojo_key}'
                    }
                ).json().get('results')[0].get('id')
            )
        except Exception as ex:
            print(f'[ERROR] {ex}')
    return result


def get_findings_from_scanners(scanner_list):
    scanners = ','.join([str(scanner) for scanner in scanner_list])
    offset = 0
    count = 10
    result = {
        'results': []
    }
    while len(result.get('results')) < count:
        response = requests.get(
            url=f'{defectdojo_url}/api/v2/findings/?duplicate=false&limit=100&found_by={scanners}'
                f'&verified=false&false_p=false&is_mitigated=false&test__engagement={engagement_id}'
                f'&not_tag=many_in_one&offset={offset}',
            headers={
                'Authorization': f'Token {defectdojo_key}'
            }
        ).json()
        count = response.get('count')
        result['results'].extend(response.get('results'))
        offset += 100
    return result


def create_test(scanner_id):
    new_test_data = {
        "engagement": engagement_id,
        "title": "Vulnerabilities simplification",
        "description": "Vulnerabilities simplification",
        "target_start": f"{datetime.now()}",
        "target_end": f"{datetime.now()}",
        "test_type": scanner_id
    }

    return requests.post(
        url=f'{defectdojo_url}/api/v2/tests/',
        headers={
            'Authorization': f'Token {defectdojo_key}'
        },
        data=new_test_data
    ).json().get('id')


def delete_findings(finding_list):
    for finding in finding_list:
        requests.request(
            method='DELETE',
            url=f'{defectdojo_url}/api/v2/findings/{finding.get("id")}',
            headers={
                'Authorization': f'Token {defectdojo_key}'
            }
        )
    return True


def attach_file(finding_id):
    # Dojo want your file to be unique-named.. :)
    payload = {
        'title': f'{datetime.now().timestamp()}-{package_filename}'
    }

    files = {
        'file': (package_filename, open(package_filepath, 'rb'), 'text/plain')
    }

    result = requests.request(
        method='POST',
        url=f'{defectdojo_url}/api/v2/findings/{finding_id}/files/',
        headers={
            'Authorization': f'Token {defectdojo_key}'
        },
        files=files,
        data=payload
    )
    print(result.text)
    return result


def create_super_finding(finding_list, test_id, scanner_id):
    finding_title = 'Many vulnerabilities in project dependencies'
    finding_description = f"""
        Many vulnerabilities have been detected by security pipelines in {package_filename}
        To fix them - push this file in attachments to the project. (Change it's name if required)

        You may use these commands to prevent such vulnerabilities:

        Python:

            ```
            pip3 install pip_audit
            python3 -m pip_audit -r requirements.txt --fix
            ```

        Nodejs:

            ```
            npm audit fix --force
            ```

        Golang:

            ```
            go get -u && go mod tidy
            ```

        The list of vulnerabilities:


    """
    for finding in finding_list:
        finding_description += f'- [{finding.get("severity")}] {finding.get("title")}\n'

    new_finding_data = {
        "found_by": [
            scanner_id
        ],
        "title": finding_title,
        "test": test_id,
        "severity": "High",
        "description": finding_description,
        "mitigation": "Update/downgrade/change libraries",
        "active": True,
        "verified": True,
        "numerical_severity": "S4",
        "tags": [
            "many_in_one"
        ],
    }

    return requests.post(
        url=f'{defectdojo_url}/api/v2/findings/',
        headers={
            'Authorization': f'Token {defectdojo_key}'
        },
        data=new_finding_data
    )


print("[Start] First get scanner ids. It's just required for next functions")
ids = get_scanners_ids()
print(f"[Done] First get scanner ids. ids = {ids}")
print("[Start] Getting vulnerabilities")
findings = get_findings_from_scanners(scanner_list=ids).get('results')
print(f"[Done] Getting vulnerabilities. {len(findings)}")
print("[Start] Getting test_id")
test_id = create_test(scanner_id=ids[0])
print(f"[Done] Getting test_id = {test_id}")
print("Oh god, we are finally ready to push our super-finding")
print("[Start] Finding creation")
new_finding_response = create_super_finding(
    finding_list=findings,
    test_id=test_id,
    scanner_id=ids[0]
)
print(f"[{new_finding_response.status_code}] Finding creation")
if str(attach_file_flag).lower() == 'true':
    print("[Start] Sending the patched file")
    attach_file_reponse = attach_file(finding_id=new_finding_response.json().get('id'))
    print(f"[{attach_file_reponse.status_code}] Sending the patched file")

if new_finding_response.status_code == 200 or new_finding_response.status_code == 201:
    print("Everything was OK, so let's drop other findings as we have so cool one")
    delete_findings(finding_list=findings)

    print(f"Done. Here is that big guy: {defectdojo_url}/finding/{new_finding_response.json().get('id')}")

print('Bye')

# Gosec

Inspects source code for security problems by scanning the Go AST.

https://github.com/securego/gosec